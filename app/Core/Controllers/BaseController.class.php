<?php

namespace Core\Controllers;


use Core\Routing\Request;
use Core\Routing\Response;

/**
 * Basic Controller template. This defines the possible options for a controller, so if an extension does not include it
 * the exception method in this controller will catch any attempts to use it.
 *
 * @package Core\Controllers
 */
class BaseController{

    public function handleGet(Request $request, Response $response, array $args){
        throw new \Exception('Could not handle request using this method (GET)', 405);
    }

    public function handlePost(Request $request, Response $response, array $args){
        throw new \Exception('Could not handle request using this method (POST)', 405);
    }

    public function handlePut(Request $request, Response $response, array $args){
        throw new \Exception('Could not handle request using this method (PUT)', 405);
    }

    public function handleDelete(Request $request, Response $response, array $args){
        throw new \Exception('Could not handle request using this method (DELETE)', 405);
    }

}
